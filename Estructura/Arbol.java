package Estructura;

import java.util.ArrayList;
import java.util.List;

/**
 * El tipo de arbol que escojí utiliza balanceos para evitar 
 * que la altura de los subarboles sea muy diferentes.
 * Lo que pretendemos es que cresca en anchura antes que en profundidad.  
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Arbol {
    private int altura(Nodo N) {
        if (N == null)
            return 0;
        return N.altura;
    }
    
    
    /*
    *El metodo de insertar utiliza la verificacion del balance en los nodos
    *para poder hacer la rotaciones y evitar que el arbol cresca en profundidad.    
    */
    public Nodo insertar(Nodo nodo, int valor) {        
        if (nodo == null) {
            return (new Nodo(valor));
        }

        if (valor < nodo.valor)
            nodo.izquierdo  = insertar(nodo.izquierdo, valor);
        else
            nodo.derecho = insertar(nodo.derecho, valor);

        
        nodo.altura = Math.max(altura(nodo.izquierdo), altura(nodo.derecho)) + 1;

        /* 
         *Obtenemos el balance para verificar si necesita rotacion
         */
        int balance = obtenerBalance(nodo);

        // Existen 4 casos para la rotacion

        // Caso izquierdo izquierdo
        if (balance > 1 && valor < nodo.izquierdo.valor)
            return rotacionDerecha(nodo);

        // Caso derecho derecho
        if (balance < -1 && valor > nodo.derecho.valor)
            return rotacionIzquierda(nodo);

        // Caso izquierdo derecho
        if (balance > 1 && valor > nodo.izquierdo.valor)
        {
            nodo.izquierdo =  rotacionIzquierda(nodo.izquierdo);
            return rotacionDerecha(nodo);
        }

        // Caso derecho izquierdo
        if (balance < -1 && valor < nodo.derecho.valor)
        {
            nodo.derecho = rotacionDerecha(nodo.derecho);
            return rotacionIzquierda(nodo);
        }
        
        return nodo;
    }
    
    
    //Se utiliza el balance para saber si el arbol binario está equilibrado.
    private int obtenerBalance(Nodo N) {
        if (N == null)
            return 0;
        return altura(N.izquierdo) - altura(N.derecho);
    }
    
    
    ///Metodos para la rotación.
    /////////////////////////
    private Nodo rotacionDerecha(Nodo y) {
        System.out.println("-- Rotacion Derecha.--");
        Nodo x = y.izquierdo;
        Nodo T2 = x.derecho;
        
        x.derecho = y;
        y.izquierdo = T2;
       
        y.altura = Math.max(altura(y.izquierdo), altura(y.derecho))+1;
        x.altura = Math.max(altura(x.izquierdo), altura(x.derecho))+1;
        
        return x;
    }
    
    private Nodo rotacionIzquierda(Nodo x) {
        System.out.println("--Rotación Izquierda--");
        Nodo y = x.derecho;
        Nodo T2 = y.izquierdo;
        
        y.izquierdo = x;
        x.derecho = T2;
        
        x.altura = Math.max(altura(x.izquierdo), altura(x.derecho))+1;
        y.altura = Math.max(altura(y.izquierdo), altura(y.derecho))+1;
        
        return y;
    }
    /////////////////////////////////////
    //Metodo de impresion bien chafa jaja.    
    public void preOrder(Nodo raiz) {
        if (raiz != null) {
            preOrder(raiz.izquierdo);
            System.out.printf("%d ", raiz.valor);
            preOrder(raiz.derecho);
        }
    }
    
    private Nodo valorMinimo(Nodo nodo) {
        Nodo actual = nodo;        
        while (actual.izquierdo != null)
            actual = actual.izquierdo;
        return actual;
    }
    
    
    //En el metodo de eliminacion, necesitamos poder balancear el arbol     
    public Nodo eliminar(Nodo raiz, int valor) {        

        if (raiz == null)
            return raiz;

        
        if ( valor < raiz.valor )
            raiz.izquierdo = eliminar(raiz.izquierdo, valor);

        
        else if( valor > raiz.valor )
            raiz.derecho = eliminar(raiz.derecho, valor);

        
        else {
            
            if( (raiz.izquierdo == null) || (raiz.derecho == null) ) {

                Nodo temp;
                if (raiz.izquierdo != null)
                        temp = raiz.izquierdo;
                else
                    temp = raiz.derecho;

               
                if(temp == null) {
                    temp = raiz;
                    raiz = null;
                }
                else
                    raiz = temp; 

                temp = null;
            }
            else {
             
                Nodo temp = valorMinimo(raiz.derecho);

                raiz.valor = temp.valor;

                raiz.derecho = eliminar(raiz.derecho, temp.valor);
            }
        }

       
        if (raiz == null)
            return raiz;

        
        raiz.altura = Math.max(altura(raiz.izquierdo), altura(raiz.derecho)) + 1;

        
        int balance = obtenerBalance(raiz);

        // 4 casos para la rotacion

        //caso izquierdo izquierdo 
        if (balance > 1 && obtenerBalance(raiz.izquierdo) >= 0)
            return rotacionDerecha(raiz);

        // caso izquierdo derecho 
        if (balance > 1 && obtenerBalance(raiz.izquierdo) < 0) {
            raiz.izquierdo =  rotacionIzquierda(raiz.izquierdo);
            return rotacionDerecha(raiz);
        }

        //caso derecho derecho 
        if (balance < -1 && obtenerBalance(raiz.derecho) <= 0)
            return rotacionIzquierda(raiz);

        // caso derecho izquierdo 
        if (balance < -1 && obtenerBalance(raiz.derecho) > 0) {
            raiz.derecho = rotacionDerecha(raiz.derecho);
            return rotacionIzquierda(raiz);
        }

        return raiz;
    }
    /////////////////////////////////////////////////////////////
    //Imprime el arbol de representacion de niveles e hijos./// Pretty cool. )
    public void pintarArbolito(Nodo raiz) {
        if(raiz == null) {
            System.out.println("(raiz nula)");
            return;
        }

        int altura = raiz.altura,
            anchura = (int)Math.pow(2, altura-1);

        
        List<Nodo> actual = new ArrayList<>(1),
            next = new ArrayList<>(2);
        actual.add(raiz);

        final int maxHalfLength = 4;
        int elementos = 1;

        StringBuilder sb = new StringBuilder(maxHalfLength*anchura);
        for(int i = 0; i < maxHalfLength*anchura; i++)
            sb.append(' ');
        String textBuffer;

        
        for(int i = 0; i < altura; i++) {

            sb.setLength(maxHalfLength * ((int)Math.pow(2, altura-1-i) - 1));

            
            textBuffer = sb.toString();

            
            for(Nodo n : actual) {

                System.out.print(textBuffer);

                if(n == null) {

                    System.out.print("        ");
                    next.add(null);
                    next.add(null);

                } else {

                    System.out.printf("(%6d)", n.valor);
                    next.add(n.izquierdo);
                    next.add(n.derecho);

                }

                System.out.print(textBuffer);

            }

            System.out.println();
            // Print tree node extensions for next level.
            if(i < altura - 1) {

                for(Nodo n : actual) {

                    System.out.print(textBuffer);

                    if(n == null)
                        System.out.print("        ");
                    else
                        System.out.printf("%s      %s",
                                n.izquierdo == null ? " " : "/", n.derecho == null ? " " : "\\");

                    System.out.print(textBuffer);

                }

                System.out.println();

            }

            // Renewing indicators for next run.
            elementos *= 2;
            actual = next;
            next = new ArrayList<Nodo>(elementos);

        }    
    }
    
    public Nodo buscar(Nodo raiz,int valor){
        Nodo auxiliar = raiz;
        while(auxiliar.valor != valor){
            if(valor<auxiliar.valor){
                auxiliar = auxiliar.izquierdo;
            }else{
                auxiliar = auxiliar.derecho;
            }
            
            if(auxiliar == null)
                return null;
        }
        return auxiliar;
    }
   
}
