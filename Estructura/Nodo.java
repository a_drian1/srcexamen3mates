package Estructura;

/**
 * 
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Nodo {
   //Se guardan las referencias al hijo izquierdo, hijo derecho y el padre.
   public Nodo izquierdo, derecho, padre;
   public int altura = 1;
   public int valor;

   Nodo (int val) {
       this.valor = val;
   } 
}
