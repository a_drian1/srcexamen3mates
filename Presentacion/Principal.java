package Presentacion;

import Estructura.Arbol;
import Estructura.Nodo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Tema:
 * Binary Serarch Tree (BST) 
 * Exámen Tercer Parcial.
 * @author Adrián Ibarra González a_drian1@hotmail.es
 */
public class Principal {
    public static void main(String[] args){
        System.out.println("-----------------------------------------------");
        System.out.println("--EXAMEN 3er PARCIAL DE MATEMATICAS DISCRETAS--");
        System.out.println("-------AUTOR: ADRIÁN IBARRA GONZÁLEZ.----------");
        System.out.println("-----------------------------------------------\n");
        Arbol t = new Arbol();
        Nodo root = null;   
        Nodo nodo = null;
        while (true) {
            System.out.println("-------------------");
            System.out.println("(1) Insertar");
            System.out.println("(2) Eliminar");
            System.out.println("(3) Buscar");
            System.out.println("(4) Imprimir arbol");
            System.out.println("(5) Salir");
            System.out.println("Elije una opción:");
            System.out.println("-------------------");
            
           
            try {
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                String s = bufferRead.readLine();                
                switch (Integer.parseInt(s)) {
                    case 1:
                        System.out.print("Valor a insertar: ");
                        int insertar = Integer.parseInt(bufferRead.readLine());
                        if(root != null){
                            if(t.buscar(root,insertar)==null ){
                                root = t.insertar(root, insertar);
                                System.out.println();
                                t.pintarArbolito(root);
                                System.out.println();
                            }else{
                                System.out.println("Ya existe el nodo...");
                            }
                        }else{
                            root = t.insertar(root, insertar);
                            System.out.println();
                            t.pintarArbolito(root);
                            System.out.println();
                        }
                        
                        break;
                    case 2:
                        if(root == null){
                            System.out.println("No existen nodos en el arbol...");
                            break;
                        }
                        System.out.print("Valor a eliminar: ");
                        int eliminar = Integer.parseInt(bufferRead.readLine());
                        if(root!=null){
                            if(t.buscar(root, eliminar)!=null){
                                root = t.eliminar(root, eliminar);   
                                t.pintarArbolito(root);
                                System.out.println("El valor "+ eliminar+" fue eliminado...");
                            }else{
                                System.out.println("No existe el nodo a eliminar...");
                            }
                        }                                                                                                                       
                        break;
                    case 3:
                        
                        if(root != null){
                            System.out.print("Valor a buscar: ");
                            nodo = t.buscar(root, Integer.parseInt(bufferRead.readLine()));                            
                        }else{
                            System.out.println("No existen nodos en el arbol...");
                            break;
                        }
                        
                        if(nodo != null){                           
                            System.out.println("(  "+nodo.valor+") Nodo Encontrado¡¡¡");
                        }else{
                            System.out.println("Nodo no encontrado :(");
                        }
                        break;
                    case 4:
                        t.pintarArbolito(root);
                        break;
                    case 5:
                        System.exit(0);
                    default:
                        System.out.println("Opcion No válida");
                        continue;
                }

                
            }
            catch(Exception e) {   
                System.out.println("Error al ingresar los datos. Intente nuevamente.");
            }
            
            System.out.println("ENTER PARA CONTINUAR...");
            try{System.in.read();}catch(IOException e){}              
        }
    }    
}    

